package perfectreport_jobs

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core/gg_log"
	"bitbucket.org/digi-sense/gg-core/gg_scheduler"
	"bitbucket.org/digi-sense/gg-progr-perfectreport/perfectreport/perfectreport_commons"
	"bitbucket.org/digi-sense/gg-progr-perfectreport/perfectreport/perfectreport_engine"
	"fmt"
)

type PerfectReportJob struct {
	name        string
	jobRoot     string
	jobSettings *perfectreport_commons.JobSettings
	postman     *perfectreport_commons.PerfectReportPostman

	root         string
	jobScheduler *gg_scheduler.Scheduler
	logger       *gg_log.Logger
	started      bool
	lastError    error

	engine *perfectreport_engine.PerfectReportEngine
}

func NewPerfectReportJob(name string, jobRoot string, jobSettings *perfectreport_commons.JobSettings, postman *perfectreport_commons.PerfectReportPostman) *PerfectReportJob {
	instance := new(PerfectReportJob)
	instance.name = name
	instance.jobRoot = jobRoot
	instance.jobSettings = jobSettings
	instance.postman = postman

	instance.root = gg.Paths.Concat(instance.jobRoot, name)

	// personal logger
	logFile := gg.Paths.Concat(gg.Paths.WorkspacePath("logging"), instance.jobSettings.Uid+".log")
	_ = gg.IO.Remove(logFile) // reset at init if any
	instance.logger = gg_log.NewLogger()
	instance.logger.SetFilename(logFile)

	if len(instance.jobSettings.Uid) > 0 {

		// load schedule settings
		schedule := instance.jobSettings.Schedule
		if len(schedule.Timeline) > 0 {
			instance.jobScheduler = gg_scheduler.NewScheduler()
			instance.jobScheduler.AddSchedule(&gg_scheduler.Schedule{
				Uid:       schedule.Uid,
				StartAt:   schedule.StartAt,
				Timeline:  schedule.Timeline,
				Payload:   schedule.Payload,
				Arguments: schedule.Arguments,
			})
			instance.jobScheduler.OnSchedule(func(schedule *gg_scheduler.SchedulerTask) {
				instance.jobScheduler.Pause()
				defer instance.jobScheduler.Resume()
				_, _ = instance.doJob()
			})
			// scheduler enabled
			instance.logger.Info(fmt.Sprintf("Report '%s' scheduler enabled.", instance.name))

		} else {
			// scheduler not enabled
			instance.logger.Info(fmt.Sprintf("Report '%s' scheduler NOT enabled.", instance.name))

		}

		// engine
		instance.engine = perfectreport_engine.NewReportEngine(instance.root, instance.name, instance.logger,
			instance.jobSettings.Dataset, instance.postman, instance.jobSettings.Output)
	}

	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//		p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *PerfectReportJob) IsScheduled() bool {
	return nil != instance && nil != instance.jobScheduler
}

func (instance *PerfectReportJob) IsValid() bool {
	if nil != instance.jobSettings {
		return len(instance.jobSettings.Uid) > 0
	}
	return false
}

func (instance *PerfectReportJob) IsEnabled() bool {
	if nil != instance.jobSettings {
		return instance.jobSettings.Enabled
	}
	return false
}

func (instance *PerfectReportJob) GetName() string {
	if nil != instance.jobSettings {
		return instance.name
	}
	return ""
}

func (instance *PerfectReportJob) GetUid() string {
	if nil != instance.jobSettings {
		return instance.jobSettings.Uid
	}
	return ""
}

func (instance *PerfectReportJob) Start() (bool, error) {
	if instance.IsEnabled() {
		instance.jobScheduler.Start()
		// instance.report.Start()
		return true, nil
	}
	return false, nil
}

func (instance *PerfectReportJob) Stop() {
	instance.jobScheduler.Stop()
	// instance.report.Stop()
	return
}

func (instance *PerfectReportJob) Run() (files []string, err error) {
	return instance.doJob()
}

// ---------------------------------------------------------------------------------------------------------------------
//		p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *PerfectReportJob) doJob() (files []string, err error) {
	if nil != instance.engine {
		return instance.engine.Run()
	}
	return
}
