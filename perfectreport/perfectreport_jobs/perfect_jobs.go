package perfectreport_jobs

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core/gg_events"
	"bitbucket.org/digi-sense/gg-progr-perfectreport/perfectreport/perfectreport_commons"
	"errors"
	"fmt"
)

// PerfectReportJobs main controller that run jobs
type PerfectReportJobs struct {
	root    string
	logger  *perfectreport_commons.Logger
	events  *gg_events.Emitter
	postman *perfectreport_commons.PerfectReportPostman

	jobs map[string]*PerfectReportJob
}

func NewPerfectReportJobs(dirWork string, logger *perfectreport_commons.Logger, events *gg_events.Emitter, postman *perfectreport_commons.PerfectReportPostman) (*PerfectReportJobs, error) {
	instance := new(PerfectReportJobs)
	instance.root = gg.Paths.Concat(dirWork, perfectreport_commons.JobsDir)
	instance.logger = logger
	instance.events = events
	instance.postman = postman
	instance.jobs = make(map[string]*PerfectReportJob)

	err := instance.init()

	return instance, err
}

// ---------------------------------------------------------------------------------------------------------------------
//		p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *PerfectReportJobs) Start() error {
	jobErrors := make([]string, 0)
	if len(instance.jobs) > 0 {
		for _, job := range instance.jobs {
			started, err := job.Start() // start schedule
			if nil != err {
				jobErrors = append(jobErrors, err.Error())
				instance.logger.Error(fmt.Sprintf("Job '%s' starting error: %s", job.name, err.Error()))
			} else {
				if started {
					instance.logger.Info(fmt.Sprintf("Job '%s' started.", job.name))
				}
			}
		}
	}
	if len(jobErrors) > 0 {
		return errors.New(fmt.Sprintf("%v error/s occurred starting jobs: [%s]", len(jobErrors), gg.Strings.Concat(jobErrors)))
	}
	return nil
}

func (instance *PerfectReportJobs) Stop() {
	if len(instance.jobs) > 0 {
		for _, job := range instance.jobs {
			job.Stop() // stop schedule
		}
	}
	return
}

func (instance *PerfectReportJobs) Run(name string) ([]string, error) {
	if nil != instance && nil != instance.jobs {
		if j, b := instance.jobs[name]; b {
			return j.Run()
		} else {
			return nil, gg.Errors.Prefix(perfectreport_commons.MissingJobError, name)
		}
	}
	return nil, perfectreport_commons.PanicSystemError
}

// ---------------------------------------------------------------------------------------------------------------------
//		p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *PerfectReportJobs) init() error {

	// load jobs
	list, err := gg.Paths.ListFiles(instance.root, "*.json")
	if nil != err {
		return err
	}
	for _, file := range list {
		name := gg.Paths.FileName(file, false)
		if text, e := gg.IO.ReadTextFromFile(file); e == nil && len(text) > 0 {
			var s perfectreport_commons.JobSettings
			err = gg.JSON.Read(text, &s)
			if nil == err {
				name := gg.Paths.FileName(gg.Paths.Dir(file), false)
				job := NewPerfectReportJob(name, instance.root, &s, instance.postman)
				if job.IsValid() {
					instance.jobs[job.GetUid()] = job
				}
			} else {
				if name == "job" {
					instance.logger.Error("Error on perfectreport_jobs.init()", err)
				}
			}
		}
	}

	return nil
}
