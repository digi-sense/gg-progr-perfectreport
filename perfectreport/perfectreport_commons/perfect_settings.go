package perfectreport_commons

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_sms/gg_sms_engine"
	"bitbucket.org/digi-sense/gg-core/gg_email"
	_ "embed"
	"fmt"
)

type PerfectReportSettings struct {
	SMS   *PerfectReportSettingsSMS
	Email *PerfectReportSettingsEmail
}

type PerfectReportSettingsSMS struct {
	gg_sms_engine.SMSConfiguration
}

type PerfectReportSettingsEmail struct {
	gg_email.SmtpSettings
	Enabled bool `json:"enabled"`
}

func (instance *PerfectReportSettingsEmail) String() string {
	return gg.JSON.Stringify(instance)
}

func NewSettings(mode string) (*PerfectReportSettings, error) {
	instance := new(PerfectReportSettings)
	err := instance.init(mode)

	return instance, err
}

func (instance *PerfectReportSettings) init(mode string) error {
	if len(mode) == 0 {
		mode = "production"
	}
	dir := gg.Paths.WorkspacePath("./")
	filename := gg.Paths.Concat(dir, fmt.Sprintf("settings.%s.json", mode))

	// load settings
	return gg.JSON.ReadFromFile(filename, &instance)
}
