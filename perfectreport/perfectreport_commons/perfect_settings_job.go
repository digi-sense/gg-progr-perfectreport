package perfectreport_commons

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core/gg_scheduler"
	_ "embed"
)

type JobSettings struct {
	Uid      string       `json:"uid"`
	Enabled  bool         `json:"enabled"`
	Schedule *JobSchedule `json:"schedule"`
	Dataset  *JobDataset  `json:"dataset"`
	Output   *JobOutput   `json:"output"`
}

type JobDataset struct {
	Script  *JobActionScript `json:"script"` // optional js script to launch
	Master  *JobDatasource   `json:"master"`
	Details []*JobDatasource `json:"details"`
}

type JobDatasource struct {
	Name     string           `json:"name"`
	Script   *JobActionScript `json:"script"` // optional js script to launch
	FlatData bool             `json:"flat_data"`
	Data     []interface{}    `json:"data"` // optional data
	Database *JobDatabase     `json:"database"`
}

type JobDatabase struct {
	Driver string `json:"driver"`
	Dsn    string `json:"dsn"`
	Query  string `json:"query"`
}

type JobSchedule struct {
	gg_scheduler.Schedule
}

type JobActionScript struct {
	OnBefore string `json:"on_before"`
	OnAfter  string `json:"on_after"`
}

type JobOutput struct {
	Template     string           `json:"template"`
	Filename     string           `json:"filename"`
	NotifyAll    bool             `json:"notify_all"`
	Notification *JobNotification `json:"notification"`
}

type JobNotification struct {
	Email *JobMailNotification `json:"email"`
	Sms   *JobSMSNotification  `json:"sms"`
}

func (instance *JobNotification) String() string {
	return gg.JSON.Stringify(instance)
}

func (instance *JobNotification) Clone() *JobNotification {
	var clone *JobNotification
	_ = gg.JSON.Read(instance.String(), &clone)
	return clone
}

type JobSMSNotification struct {
	To      string `json:"to"`
	Message string `json:"message"`
}
type JobMailNotification struct {
	JobSMSNotification
	Subject     string   `json:"subject"`
	Attachments []string `json:"attachments"`
}

func (instance *JobMailNotification) GetAttachments() []string {
	response := make([]string, 0)
	for _, a := range instance.Attachments {
		response = append(response, gg.Paths.WorkspacePath(a))
	}
	return response
}
