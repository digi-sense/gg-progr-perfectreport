package perfectreport_commons

import "errors"

const (
	AppName    = "PerfectReport"
	AppVersion = "0.1.2"

	ModeProduction = "production"
	ModeDebug      = "debug"

	DateLayout = "2006-01-02T15:04:05.000Z"
)

// ---------------------------------------------------------------------------------------------------------------------
//		e v e n t s
// ---------------------------------------------------------------------------------------------------------------------

const (
	// EventOnDoStop application is stopping
	EventOnDoStop = "on_do_stop"
)

// ---------------------------------------------------------------------------------------------------------------------
//		w o r k s p a c e s
// ---------------------------------------------------------------------------------------------------------------------

const (
	WpDirStart = "start"
	WpDirApp   = "app"
	WpDirWork  = "*"

	JobsDir = "jobs"
)

// ---------------------------------------------------------------------------------------------------------------------
//		e r r o r s
// ---------------------------------------------------------------------------------------------------------------------

var (
	PanicSystemError                = errors.New("panic_system_error")
	MissingJobError                 = errors.New("missing_job")
	TemplateFormatNotSupportedError = errors.New("template_format_not_supported")
	DatafileFormatNotSupportedError = errors.New("datafile_format_not_supported")
	FileNotFoundError               = errors.New("file_not_found")
)
