package perfectreport_engine

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core/gg_log"
	"bitbucket.org/digi-sense/gg-progr-perfectreport/perfectreport/perfectreport_commons"
	"fmt"
	"github.com/cbroglie/mustache"
)

type PerfectReportEngine struct {
	root     string
	name     string
	logger   *gg_log.Logger
	dataset  *perfectreport_commons.JobDataset
	postman  *perfectreport_commons.PerfectReportPostman
	settings *perfectreport_commons.JobOutput

	notifyAll bool
	scripting *PerfectReportScripting
}

func NewReportEngine(root, name string, l *gg_log.Logger, dataset *perfectreport_commons.JobDataset, postman *perfectreport_commons.PerfectReportPostman, settings *perfectreport_commons.JobOutput) *PerfectReportEngine {
	instance := new(PerfectReportEngine)
	instance.root = root
	instance.name = name
	instance.logger = l
	instance.dataset = dataset
	instance.postman = postman
	instance.settings = settings
	instance.notifyAll = settings.NotifyAll

	instance.scripting = NewReportScripting(instance.root, instance.logger)

	return instance
}

// Run execute report's job
func (instance *PerfectReportEngine) Run() (files []string, err error) {
	if nil != instance && nil != instance.dataset {
		return instance.run()
	}
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//		p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *PerfectReportEngine) run() (files []string, err error) {
	dataset := instance.dataset

	var onBefore, onAfter string
	if nil != dataset.Script {
		onBefore = dataset.Script.OnBefore
		onAfter = dataset.Script.OnAfter
	}

	globalContext := make(map[string]interface{})

	master := dataset.Master
	if nil != master {

		// onBefore rendering
		if len(onBefore) > 0 {
			response, e := instance.scripting.RunScript(globalContext, onBefore)
			if nil == e {
				gg.Maps.Merge(false, globalContext, response)
			} else {
				err = e
			}
		}

		err = instance.scripting.SolveDataSource(globalContext, master)
		if nil == err {
			// loop on masters for master-detail reports
			masterData := toArray(globalContext[master.Name]) // must be an array
			if len(masterData) > 0 {
				for _, data := range masterData {

					// replace the master with current master item
					globalContext[master.Name] = data

					// loop on details and creates datasets
					err = instance.scripting.SolveDataSources(globalContext, dataset.Details)
					if nil != err {
						break
					}

					// render report
					// now all datasets are ready and sored into globalContext using as key the dataset name
					// REMEMBER: "master" is always one and all other datasets are stored by name
					templateName := gg.Paths.WorkspacePath(instance.settings.Template)
					fileName := gg.Paths.Concat(instance.root, "output", instance.settings.Filename)
					outFile, e := render(templateName, fileName, globalContext)
					if nil != e {
						err = e
						break
					}

					// append to generated files
					files = append(files, outFile)
					if instance.notifyAll {
						// notify every single master report
						instance.notify([]string{outFile}, globalContext)
					}
				}
			} else {
				err = gg.Errors.Prefix(perfectreport_commons.PanicSystemError, "MASTER dataset must be an array of something!")
			}
		}

		// onAfter all rendering
		if nil == err && len(onAfter) > 0 {
			response, e := instance.scripting.RunScript(globalContext, onAfter)
			if nil == e {
				gg.Maps.Merge(false, globalContext, response)
			} else {
				err = e
			}
		}
	}

	// log error if any
	if nil != err && nil != instance.logger {
		instance.logger.Error(fmt.Sprintf("Error running report engine for job '%s': %s", instance.name, err))
	} else {
		if !instance.notifyAll {
			// notify only at the end
			instance.notify(files, globalContext)
		}
	}

	return
}

func (instance *PerfectReportEngine) notify(reportFiles []string, context map[string]interface{}) {
	if nil != instance && nil != instance.settings && nil != instance.logger {
		// PANIC RECOVERY
		defer func() {
			if r := recover(); r != nil {
				// recovered from panic
				message := gg.Strings.Format("[panic] perfect_engine.notify ERROR: %s", r)
				instance.logger.Error(message)
			}
		}()
		settings := instance.settings.Notification
		// email
		emailSettings := settings.Email
		if nil != emailSettings {
			subject, _ := mustache.Render(emailSettings.Subject, context)
			to, _ := mustache.Render(emailSettings.To, context)
			message, _ := mustache.Render(emailSettings.Message, context)
			if len(to) > 0 {
				instance.postman.NotifyEmail(to, subject, message, reportFiles, nil, false)
			}
		}

		// sms
		smsSettings := settings.Sms
		if nil != smsSettings {
			to, _ := mustache.Render(smsSettings.To, context)
			message, _ := mustache.Render(smsSettings.Message, context)
			if len(to) > 0 && len(message) > 0 {
				instance.postman.NotifySMS(to, message, nil, false)
			}
		}
	}
}
