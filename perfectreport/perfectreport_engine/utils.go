package perfectreport_engine

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-module-office/helper"
	"bitbucket.org/digi-sense/gg-progr-perfectreport/perfectreport/perfectreport_commons"
	"fmt"
	"github.com/cbroglie/mustache"
	"strings"
)

func toArray(raw interface{}) []interface{} {
	if b, v := gg.Compare.IsArray(raw); b {
		return gg.Convert.ToArray(v.Interface())
	}
	return []interface{}{raw}
}

func render(templateName, fileName string, model map[string]interface{}) (file string, err error) {
	templateName, err = mustache.Render(templateName, model)
	if nil != err {
		return
	}

	fileName, err = mustache.Render(fileName, model)
	if nil != err {
		return
	}

	file = strings.ToLower(gg.Strings.Slugify(fileName))

	if b, e := gg.Paths.Exists(templateName); b {
		ext := strings.ToLower(gg.Paths.ExtensionName(templateName))
		switch ext {
		case "doc", "docx", "odt":
			err = renderWrite(templateName, file, model)
		default:
			err = gg.Errors.Prefix(perfectreport_commons.TemplateFormatNotSupportedError, ext)
		}

	} else {
		err = e
	}
	return
}

func renderWrite(templateName, fileName string, context map[string]interface{}) (err error) {
	doc := helper.NewWrite()
	err = doc.OpenModel(templateName)
	if nil == err {
		errs := doc.Render(context)
		if len(errs) > 0 && nil != errs[0] {
			err = gg.Errors.Prefix(errs[0], fmt.Sprintf("Error rendering template '%s'", gg.Paths.FileName(templateName, true)))
		} else {
			// save rendered file
			err = doc.SaveTo(fileName)
		}
	}
	return
}
