package perfectreport_engine

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x"
	"bitbucket.org/digi-sense/gg-core-x/gg_dbal/gg_dbal_drivers"
	"bitbucket.org/digi-sense/gg-core-x/gg_scripting"
	"bitbucket.org/digi-sense/gg-core/gg_log"
	"bitbucket.org/digi-sense/gg-progr-perfectreport/perfectreport/perfectreport_commons"
	"github.com/cbroglie/mustache"
	"strings"
)

type PerfectReportScripting struct {
	root   string
	logger *gg_log.Logger
}

func NewReportScripting(root string, l *gg_log.Logger) *PerfectReportScripting {
	instance := new(PerfectReportScripting)
	instance.root = root
	instance.logger = l
	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//		p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *PerfectReportScripting) SolveDataSources(globalContext map[string]interface{}, sources []*perfectreport_commons.JobDatasource) (err error) {
	for _, detail := range sources {
		err = instance.SolveDataSource(globalContext, detail)
		if nil != err {
			break
		}
	}
	return
}

// SolveDataSource solve source and update globalContext
func (instance *PerfectReportScripting) SolveDataSource(globalContext map[string]interface{}, datasource *perfectreport_commons.JobDatasource) error {
	script := datasource.Script
	globalContext[datasource.Name] = datasource.Data

	if len(datasource.Data) > 0 {
		globalContext["tmpdata"] = datasource.Data // exposed if any
	} else {
		globalContext["tmpdata"] = false
	}

	// before datasource
	if nil != script && len(script.OnBefore) > 0 {
		response, e := instance.RunScript(globalContext, script.OnBefore)
		if nil != e {
			return e
		}
		// merge with global-context
		gg.Maps.Merge(false, globalContext, response)
	}

	// solve datasource
	if nil != datasource.Database && len(datasource.Database.Dsn) > 0 && len(datasource.Database.Driver) > 0 {
		response, err := instance.GetData(datasource.Database.Driver, datasource.Database.Dsn,
			datasource.Database.Query, globalContext)
		if nil != err {
			return err
		}

		if len(response) > 0 {
			if datasource.FlatData {
				globalContext[datasource.Name] = response[0]
			} else {
				globalContext[datasource.Name] = response
			}
			globalContext["tmpdata"] = globalContext[datasource.Name]
		}
	}

	// after master
	if nil != script && len(script.OnAfter) > 0 {
		response, e := instance.RunScript(globalContext, script.OnAfter)
		if nil != e {
			return e
		}
		// merge with global-context
		gg.Maps.Merge(false, globalContext, response)
	}

	// reset tmpdata
	globalContext["tmpdata"] = false

	return nil
}

func (instance *PerfectReportScripting) GetData(driverName, dsn, query string, globalContext map[string]interface{}) (response []interface{}, err error) {
	// get query and solve with context data if used for filtering
	query, err = mustache.Render(query, globalContext)

	switch driverName {
	case "file":
		// file system json or csv data
		fileName := gg.Paths.Concat(instance.root, strings.ReplaceAll(dsn, "file://", ""))
		if b, _ := gg.Paths.Exists(fileName); b {
			fileExt := gg.Paths.ExtensionName(fileName)
			switch fileExt {
			case "json":
				var i interface{}
				err = gg.JSON.ReadFromFile(fileName, &i)
				if nil == err {
					response, err = instance.filter(toArray(i), query)
				}
			case "csv":
				text, e := gg.IO.ReadTextFromFile(fileName)
				if nil != e {
					err = e
				} else {
					options := gg.CSV.NewCsvOptionsDefaults()
					options.FirstRowHeader = true
					options.Comma = ","
					r, re := gg.CSV.ReadAll(text, options)
					if nil != re {
						err = re
					} else {
						response, err = instance.filter(toArray(r), query)
					}
				}
			default:
				err = gg.Errors.Prefix(perfectreport_commons.DatafileFormatNotSupportedError, fileExt)
			}
		} else {
			err = gg.Errors.Prefix(perfectreport_commons.FileNotFoundError, fileName)
		}
	default:
		// database abstraction layer
		driver, e := gg_dbal_drivers.NewDatabase(driverName, dsn)
		if nil != e {
			err = e
			return
		}
		if nil != err {
			return
		}

		rawResponse, e := driver.ExecNative(query, nil)
		if nil != e {
			err = e
			return
		}
		response = toArray(rawResponse)
	}

	return
}

// CheckMatch evaluate single expression over a passed object
func (instance *PerfectReportScripting) CheckMatch(itemName string, itemValue interface{}, expression string) (bool, error) {
	if len(expression) == 0 {
		return true, nil
	}
	// PANIC RECOVERY
	defer func() {
		if r := recover(); r != nil {
			// recovered from panic
			message := gg.Strings.Format("[panic] expression '%s' ERROR: %s", expression, r)
			instance.logger.Error(message)
		}
	}()
	var engine *gg_scripting.ScriptEngine
	engine = ggx.Scripting.NewEngine("javascript")
	engine.SetLogger(instance.logger)
	engine.Set(itemName, itemValue)
	engine.Open()
	value, e := engine.RunScript(expression)
	engine.Close()
	return nil == e && value.ToBoolean(), e
}

func (instance *PerfectReportScripting) RunScript(globalContext map[string]interface{}, filename string) (map[string]interface{}, error) {
	// PANIC RECOVERY
	defer func() {
		if r := recover(); r != nil {
			// recovered from panic
			message := gg.Strings.Format("[panic] script '%s' ERROR: %s", filename, r)
			instance.logger.Error(message)
		}
	}()
	if b, e := gg.Paths.Exists(filename); b {
		program, ioErr := gg.IO.ReadTextFromFile(filename)
		if nil != ioErr {
			return nil, ioErr
		}
		var engine *gg_scripting.ScriptEngine
		engine = ggx.Scripting.NewEngine("javascript")
		engine.SetLogger(instance.logger)
		for k, v := range globalContext {
			engine.Set(k, v)
		}
		engine.Open()
		value, rtErr := engine.RunScript(program)
		engine.Close()
		if nil != rtErr {
			return nil, rtErr
		}

		return gg.Convert.ToMap(value.Export()), nil
	} else {
		return nil, e
	}
}

// ---------------------------------------------------------------------------------------------------------------------
//		p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *PerfectReportScripting) filter(data []interface{}, filter string) (response []interface{}, err error) {
	for _, item := range data {
		passed, e := instance.CheckMatch("doc", item, filter)
		if nil != e {
			err = e
		} else if passed {
			response = append(response, item)
		}
	}
	return
}
