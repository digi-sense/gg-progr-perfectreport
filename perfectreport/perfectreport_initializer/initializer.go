package perfectreport_initializer

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-progr-perfectreport/perfectreport/perfectreport_commons"
	_ "embed"
	"fmt"
)

//go:embed default_template.docx
var DefaultTemplate string

//go:embed default_settings.json
var DefaultSettings string

//go:embed default_job.json
var DefaultJob string

func Initialize(mode string) (err error) {
	wpRoot := gg.Paths.WorkspacePath("./")
	tmpRoot := gg.Paths.Concat(wpRoot, "temp")

	gg.Paths.SetTempRoot(tmpRoot)
	gg.IO.RemoveAllSilent(tmpRoot)

	// settings
	filename := gg.Paths.Concat(wpRoot, fmt.Sprintf("settings.%s.json", mode))
	// ensure settings exists
	_ = gg.Paths.Mkdir(filename)
	if b, _ := gg.Paths.Exists(filename); !b {
		_, _ = gg.IO.WriteTextToFile(DefaultSettings, filename)
	}

	// create sample job
	sampleFile := gg.Paths.Concat(wpRoot, perfectreport_commons.JobsDir, "./sample/job.json")
	err = gg.Paths.Mkdir(sampleFile)
	if nil != err {
		return err
	}
	if b, _ := gg.Paths.Exists(sampleFile); !b {
		_, err = gg.IO.WriteTextToFile(DefaultJob, sampleFile)
		if nil != err {
			return err
		}
	}

	// templates
	templateRoot := gg.Paths.WorkspacePath("./templates")
	templateFile := gg.Paths.Concat(templateRoot, "default_template.docx")
	err = gg.Paths.Mkdir(templateRoot)
	if b, _ := gg.Paths.Exists(templateFile); !b {
		_, _ = gg.IO.WriteTextToFile(DefaultTemplate, templateFile)
	}

	return
}
