package perfectreport

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core/gg_events"
	"bitbucket.org/digi-sense/gg-progr-perfectreport/perfectreport/perfectreport_commons"
	"bitbucket.org/digi-sense/gg-progr-perfectreport/perfectreport/perfectreport_initializer"
	"bitbucket.org/digi-sense/gg-progr-perfectreport/perfectreport/perfectreport_jobs"
	"fmt"
	"path/filepath"
	"time"
)

// ---------------------------------------------------------------------------------------------------------------------
//		A p p l i c a t i o n
// ---------------------------------------------------------------------------------------------------------------------

type LoadZilla struct {
	mode    string
	root    string
	dirWork string

	settings    *perfectreport_commons.PerfectReportSettings
	logger      *perfectreport_commons.Logger
	stopChan    chan bool
	stopMonitor *stopMonitor
	events      *gg_events.Emitter

	postman *perfectreport_commons.PerfectReportPostman
	jobs    *perfectreport_jobs.PerfectReportJobs
}

// ---------------------------------------------------------------------------------------------------------------------
//		p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *LoadZilla) Start() (err error) {
	instance.stopChan = make(chan bool, 1)
	if nil != instance.stopMonitor {
		instance.stopMonitor.Start()
	}

	if nil != instance.jobs {
		err = instance.jobs.Start()
	}

	return
}

// Stop Try to close gracefully
func (instance *LoadZilla) Stop() (err error) {
	if nil != instance.stopMonitor {
		instance.stopMonitor.Stop()
	}

	if nil != instance.jobs {
		instance.jobs.Stop()
	}

	time.Sleep(3 * time.Second)
	if nil != instance.stopChan {
		instance.stopChan <- true
		instance.stopChan = nil
	}
	return
}

// Exit application
func (instance *LoadZilla) Exit() (err error) {
	if nil != instance.stopMonitor {
		instance.stopMonitor.Stop()
	}
	if nil != instance.stopChan {
		instance.stopChan <- true
		instance.stopChan = nil
	}

	return
}

func (instance *LoadZilla) Join() {
	if nil != instance.stopChan {
		<-instance.stopChan
	}
}

func (instance *LoadZilla) Run(name string) (files []string, err error) {
	if nil != instance.jobs {
		files, err = instance.jobs.Run(name)
	}
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//		p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *LoadZilla) doStop(_ *gg_events.Event) {
	_ = instance.Exit()
}

// ---------------------------------------------------------------------------------------------------------------------
//		S T A T I C
// ---------------------------------------------------------------------------------------------------------------------

func LaunchApplication(mode, cmdStop string, args ...interface{}) (instance *LoadZilla, err error) {
	instance = new(LoadZilla)
	instance.mode = mode

	// paths
	instance.dirWork = gg.Paths.GetWorkspace(perfectreport_commons.WpDirWork).GetPath()
	instance.root = filepath.Dir(instance.dirWork)

	// initialize environment
	err = perfectreport_initializer.Initialize(mode)
	if nil != err {
		return
	}

	instance.settings, err = perfectreport_commons.NewSettings(mode)
	if nil == err {
		instance.events = gg.Events.NewEmitter(perfectreport_commons.AppName)
		instance.stopMonitor = newStopMonitor([]string{instance.root, instance.dirWork}, cmdStop, instance.events)
		instance.events.On(perfectreport_commons.EventOnDoStop, instance.doStop)

		// logger as first parameter
		l := gg.Arrays.GetAt(args, 0, nil)
		instance.logger = perfectreport_commons.NewLogger(mode, l)

		// POSTMAN
		instance.postman, err = perfectreport_commons.NewPostman(instance.settings, instance.logger)
		if nil == err {
			// JOBS
			instance.jobs, err = perfectreport_jobs.NewPerfectReportJobs(instance.dirWork, instance.logger, instance.events, instance.postman)
			if nil == err {
				// log started
				instance.logger.Info(fmt.Sprintf("%s %s is running.", perfectreport_commons.AppName, perfectreport_commons.AppVersion))
			}
		}
	}

	return
}
