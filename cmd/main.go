package main

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-progr-perfectreport/perfectreport"
	"bitbucket.org/digi-sense/gg-progr-perfectreport/perfectreport/perfectreport_commons"
	"flag"
	"fmt"
	"log"
	"os"
)

func main() {
	// PANIC RECOVERY
	defer func() {
		if r := recover(); r != nil {
			// recovered from panic
			message := gg.Strings.Format("[panic] APPLICATION '%s' ERROR: %s", perfectreport_commons.AppName, r)
			log.Fatalf(message)
		}
	}()

	//-- command flags --//
	// run
	cmdRun := flag.NewFlagSet("run", flag.ExitOnError)
	dirWork := cmdRun.String("dir_work", gg.Paths.Absolute("./_workspace"), "Set a particular folder as main workspace")
	mode := cmdRun.String("m", perfectreport_commons.ModeProduction, "Mode allowed: 'debug' or 'production'")
	quit := cmdRun.String("s", "stop", "Quit Command: Write a command (ex: 'stop') to enable stop mode")
	job := cmdRun.String("j", "", "Job Name: Write a job name (ex: 'report1') to run a single job")

	// parse
	if len(os.Args) > 1 {
		cmd := os.Args[1]
		switch cmd {
		case "run":
			_ = cmdRun.Parse(os.Args[2:])
		default:
			panic("Command not supported: " + cmd)
		}
	} else {
		panic("Missing command. i.e. 'run'")
	}

	initialize(dirWork, mode)

	app, err := perfectreport.LaunchApplication(*mode, *quit, *job)
	if nil == err {
		if len(*job) == 0 {
			err = app.Start()
			if nil != err {
				log.Panicf("Error starting %s: %s", perfectreport_commons.AppName, err.Error())
			} else {
				// app is running
				app.Join()
			}
		} else {
			var files []string
			files, err = app.Run(*job)
			if nil != err {
				log.Panicf("Error launching %s report '%s': %s", perfectreport_commons.AppName, *job, err.Error())
			} else {
				fmt.Println(fmt.Sprintf("Runned '%s'. Output: %v", *job, files))
			}
		}
	} else {
		log.Panicf("Error launching %s: %s", perfectreport_commons.AppName, err.Error())
	}
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func initialize(dirWork *string, mode *string) {
	gg.Paths.GetWorkspace(perfectreport_commons.WpDirWork).SetPath(*dirWork)
}
