# Perfect Report #

_THE HEADLESS REPORT BUILDER_

![icon](icon_128.png)

CREATE and SCHEDULE the production of "perfect reports", just merging datasets with Word like templates.

## Features ##

Perfect Report main features are:

- Build reports using Word or LibreOffice. Uses `.doc`, `.docx`, `.odt` formats as Report Templates
- Automatic report production with internal scheduler
- On demand report production with command line
- Automatic email notification for produced reports
- Internal javascript engine for custom datasets and behaviour
- Multiple datasets support:
    - Filesystem: "json" and "csv". On both formats is supported data filtering `doc.owner=="{{header.key}}"`
    - SQL Databases: all SQL database
    - NoSQL: support ArangoDB

## Quick Start ##

[ I'm working on tutorials and documentation. Please, stay tuned. ]

## Configuration ##

Perfect Report has typically two different type of settings:

- System settings
- Job (or Report) settings

### System Settings ###

System settings allow configuring server notification providers:

- EMAIL: declare email smtp server
- SMS: declare SMS gateway

Below a `settings.production.json` system settings file:

```json
{
  "sms": {
    "enabled": false,
    "auto-short-url": false,
    "providers": {
      "default": {
        "driver": "generic",
        "method": "GET",
        "endpoint": "https://api.smshosting.it/rest/api/smart/sms/send?authKey={{auth-key}}&authSecret={{auth-secret}}&text={{message}}&to={{to}}&from={{from}}",
        "params": {
          "auth-key": "xxxxx",
          "auth-secret": "xxxx",
          "message": "",
          "to": "",
          "from": "TEST-SRV"
        },
        "headers": {}
      }
    }
  },
  "email": {
    "enabled": true,
    "secure": true,
    "host": "pro.turbo-smtp.com",
    "port": 465,
    "from": "Gian Angelo Geminiani <angelo.geminiani@ggtechnologies.sm>",
    "auth": {
      "user": "xxxxxx",
      "pass": "xxxxxx"
    }
  }
}
```

Notification providers are used to send alerts and reports.

### Report Settings ###

Reports (or Jobs) are the core of Perfect Report. Perfect Report support multiple simultaneous tasks in a multithreading
environment.

Each thread is executed in a separate environment handled with a Goroutine
(A goroutine is a lightweight thread managed by the Go runtime). A goroutine is a lightweight thread. It costs a lot
less than a real OS thread, and multiple goroutines may be multiplexed onto a single OS thread.

So, we can say that Perfect Report handle Report Jobs as an _independent concurrent thread of control within the same
address space_.

Reports (or Jobs) are directories under the master `jobs` directory. Each Perfect Report job directory contains at least
a `job.json` file.

Below a `job.json` sample file:

```json
{
  "uid": "default",
  "enabled": false,
  "schedule": {
    "start_at": "",
    "timeline": "second:2"
  },
  "dataset": {
    "script": {
      "on_before": "",
      "on_after": ""
    },
    "master": {
      "name": "header",
      "script": null,
      "database": null,
      "data": [
        {
          "index": 1,
          "key": "102568090",
          "company": "G&G Technologies S.r.l.",
          "phone": "0549 9244444",
          "name": "Gian Angelo",
          "surname": "Geminiani",
          "email": "valid@domain.com",
          "mobile": "+39 347 78...."
        }
      ]
    },
    "details": [
      {
        "name": "customer",
        "flat_data": true,
        "script": {
          "on_before": "",
          "on_after": ""
        },
        "database": {
          "driver": "arango",
          "dsn": "root:!qaz2WSX098@tcp(localhost:8529)/test)",
          "query": "FOR doc IN users  FILTER doc._key == \"{{ header.key }}\" RETURN doc"
        },
        "data": []
      },
      {
        "name": "invoice",
        "script": {
          "on_before": "",
          "on_after": ""
        },
        "database": {
          "driver": "arango",
          "dsn": "root:!qaz2WSX098@tcp(localhost:8529)/test)",
          "query": "FOR doc IN invoices  FILTER doc.owner == \"{{ header.key }}\" RETURN doc"
        },
        "data": []
      }
    ]
  },
  "output": {
    "template": "./templates/default_template.docx",
    "filename": "./{{ header.name }}-{{ header.index }}.docx",
    "notify_all": true,
    "notification": {
      "email": {
        "to": "{{ header.email }}",
        "subject": "Report is ready 'sample'",
        "message": "You have a new 'sample' report."
      },
      "sms": {
        "to": "{{ header.mobile }}",
        "message": "You have a new 'sample' report."
      }
    }
  }
}
```

Let start describing some attributes of a report configuration file.

### Schedule ###

```json
"schedule": {
"start_at": "",
"timeline": "second:2"
}
```

The `schedule` attribute define if Perfect Report must run a timed job to cyclically create and dispatch reports.

To disable this feature the field `timeline` can be left empty string.

### Dataset ###

```json
{
  "script": {
    "on_before": "",
    "on_after": ""
  },
  "master": {
    "name": "header",
    "script": null,
    "database": null,
    "data": [
      {
        "index": 1,
        "key": "102568090",
        "company": "G&G Technologies S.r.l.",
        "phone": "0549 9244444",
        "name": "Gian Angelo",
        "surname": "Geminiani",
        "email": "valid@domain.com",
        "mobile": "+39 347 78...."
      }
    ]
  },
  "details": [
    {
      "name": "customer",
      "flat_data": true,
      "script": {
        "on_before": "",
        "on_after": ""
      },
      "database": {
        "driver": "arango",
        "dsn": "root:!qaz2WSX098@tcp(localhost:8529)/test)",
        "query": "FOR doc IN users  FILTER doc._key == \"{{ header.key }}\" RETURN doc"
      },
      "data": []
    },
    {
      "name": "invoice",
      "script": {
        "on_before": "",
        "on_after": ""
      },
      "database": {
        "driver": "arango",
        "dsn": "root:!qaz2WSX098@tcp(localhost:8529)/test)",
        "query": "FOR doc IN invoices  FILTER doc.owner == \"{{ header.key }}\" RETURN doc"
      },
      "data": []
    }
  ]
}
```

Dataset has two nodes:

- master: the report main dataset.
- detail: more datasets that can be related to master fields.

#### Master ####

The **master** dataset is always a single dataset that can contain one or more records. Each "master" record is used to
begin a report production, so we will have a report for each record in a master dataset.

For example, we should need to create a report for each Sell Agent or for each Client and so on. In this case our master
dataset will contain a list of Agents or a list of Clients or some other kind of list.

TIP: If you do not need to create a master-detail report, just use "master" dataset with just some fake or empty data.
But remember: _Perfect Report generate a separated report for each record in master_.

For example here should be our "master" dataset:

| ID | NAME    | SURNAME | EMAIL                      |   |
|----|---------|---------|----------------------------|---|
| 1  | Bill    | Gatto   | bill.gatto@domain.com      |   |
| 2  | Angelo  | Gemini  | angelo.gemini@domain.com   |   |
| 3  | Antonio | Painted | antonio.painted@domain.com |   |

In above master dataset we have three records.

Perfect Report will generate three separated reports. If you do not need three separated reports you can simply create a
single record dataset with some of your data:

```
"data": [
          {
            "index": 1,
            "key": "102568090",
            "company": "G&G Technologies S.r.l.",
            "phone": "0549 9244444",
            ....
          }
        ]
```

One record = 1 single report.

Fields of the "master" dataset can be used also as parameters for report names
or sending addresses.

TIP: Perfect Report support "mustache like" syntax for attribute value customization.
For example `"filename": "./{{ header.company }}-{{ header.index }}.docx"`. In this example "header" is the name of our master 
dataset. The report file name will be "G&G-Technologies-Srl.docx".

#### Detail ####

Detail dataset may be related to master one. Obviously if you do not need to 
relate master-detail you just ignore to apply a filter to a query in dataset attributes.

While "master" contains a single dataset, "detail" contains an array of datasets.

### Anatomy of a Dataset ###

Datasets are basically declarations containing properties that explain to Perfect Report how to 
get data.
Here is a dataset declaration:

```json
{
      "name": "customer",
      "flat_data": true,
      "script": {
        "on_before": "",
        "on_after": ""
      },
      "database": {
        "driver": "arango",
        "dsn": "root:!qaz2WSX098@tcp(localhost:8529)/test)",
        "query": "FOR doc IN users  FILTER doc._key == \"{{ header.key }}\" RETURN doc"
      },
      "data": []
    }
```

Above example uses ArangoDB, but you can use any SQL database you like.

Dataset data can be explicit or implicit. Explicit data are simple declared into `data` property, as we did into our "master" dataset example just few lines above.

Implicit data are declared into `database` property. 

TIP: Perfect Report support both data files `"driver": "file","dsn": "file://./data/mydata.csv"` (json, csv) and database connections (SQL, ArangoDB).

## Advanced Usage ##

Perfect Report is not just limited at the behaviour schema described.

Basic behaviour is:
- Schedule reports production or run a single report via command line
- Create Master-Detail reports or single reports
- Write reports to files
- Send reports to email
- Notify report with SMS

But maybe you need something different or extend this behaviour.

You can do this using javascript and Perfect Report Events.

## Use it as COMMAND LINE tool ##

Perfect Report can be launched using many parameters to customize basic behaviour.

The standard behaviour is run and stay alive to accomplish scheduled jobs.

`perfectreport run`

This command assumes that the workspace is `"./_workspace"` and that Perfect Report must
execute all scheduled tasks (if any).

But you can customize basic behaviour changing some parameters:

 - `-dir_work`: change default workspace
 - `-m`: change default mode. Default value is `"production"`, but if you need more verbose logs you can change to `"debug"`
 - `-s`: change default QUIT command. Default quit command is `"stop"`
 - `-j`: run only specified report. I.E. `perfectreport run -dir_work=./_custom_workspace -m=debug -j=file-sample`



## Binaries ##

Perfect Reports works on Servers or on simple PCs, but even on Raspberry.

Supported OS are:

- Windows
- Mac OSX
- Linux
- Embedded Linux

Click here to download [Binaries](./_build).

## License ##

MIT License.

Copyright 2021 Gian Angelo Geminiani

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
