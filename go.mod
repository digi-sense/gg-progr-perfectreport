module bitbucket.org/digi-sense/gg-progr-perfectreport

go 1.16

replace bitbucket.org/digi-sense/gg-lib-docs => ../gg-lib-docs

require (
	bitbucket.org/digi-sense/gg-core v0.1.91
	bitbucket.org/digi-sense/gg-core-x v0.1.91
	bitbucket.org/digi-sense/gg-module-office v0.0.0-00010101000000-000000000000
	github.com/cbroglie/mustache v1.3.1
)
